# Introduction to On-booard Software

Read the handbook online at https://olmanqj.gitlab.io/intro-obsw-handbook

## Documentation Server
This handbook was elaborated using the Mkdocs documentation generator.
Run the server in your PC as follows:

Install dependencies
~~~
 pip install -r requirements.txt
~~~


Run the server:
~~~
mkdocs serve
~~~
